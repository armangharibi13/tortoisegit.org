User-agent: *
Disallow: /*.asc$
Disallow: /*.msi$
Disallow: /*.7z$
Disallow: /*.exe$
Disallow: /*.msp$
